<?php

/*
 * Esse arquivo faz parte de <MasterMundi/Master MDR>
 * (c) Bruno Martins brunoluiz[at]gmail.com
 *
 */

namespace Pagamentos\Braziliex\Http\Controllers;

use App\Models\Pedidos;
use App\Services\Pagamentos;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Pagamentos\Braziliex\Services\BraziliexService;

/**
 * Class BraziliexController.
 */
class BraziliexController extends Controller
{
    /**
     * @param Pedidos $pedido
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function index(Pedidos $pedido)
    {
        return view('braziliex::index', [
            'title' => 'Pagamento pedido #'.$pedido->id,
            'pedido' => $pedido,
        ]);
    }

    /**
     * @param Pedidos $pedido
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addr(Pedidos $pedido, Request $request)
    {
        if (BraziliexService::setAddress($pedido, $request->get('address'))) {
            return response()->json([], 200);
        }

        return response()->json([], 500);
    }

    /**
     * @param Pedidos $pedido
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function callback(Pedidos $pedido, Request $request)
    {
        if (BraziliexService::checkAddress($pedido)) {
            return response()->json(['payment' => true], 200);
        }

        return response()->json(['payment' => false], 500);
    }
}
