@extends('default.layout.main')

@section('content')

    @include('default.errors.errors')

    <section class="content-header">
        <h1>
            Braziliex - Bitcoin
        </h1>

        <ol class="breadcrumb hidden-xs">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Braziliex - Bitcoin</li>
        </ol>
    </section>

    <section class="content">
        <div class="col-md-12">
            <div class="receive_crypto"
                 key="{{ config('braziliex.key') }}"
                 value_brl="{{ $pedido->valor_total }}"
                 id_order="{{ $pedido->id }}"
                 description="Pedido#{{ $pedido->id }}">
            </div>
        </div>
        <div class="col-md-12" style="text-align: center; margin-top: 10px;">
            <a href="{{ redirect()->back()->getTargetUrl() }}" class="btn btn-primary"><i class="fa fa-mail-reply"></i> Voltar</a>
        </div>
    </section>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert/sweetalert2.min.css') }}">
@endsection

@section('script')
    <script src="{{ asset('plugins/sweetalert/sweetalert2.js') }}" type="text/javascript"></script>
    <script src="https://braziliex.com/js/gateway_embed.js" type="text/javascript"></script>
    <script>
        (function ($) {
            $.each(['show', 'hide'], function (i, ev) {
                var el = $.fn[ev];
                $.fn[ev] = function () {
                    this.trigger(ev);
                    return el.apply(this, arguments);
                };
            });
        })(jQuery);

        $(document).ready(function (){
            $('#fase_10').on({
                show: function () {
                    $.ajax({
                        url: '{{ route('pagamento.braziliex.addr', [ $pedido->id ]) }}',
                        type: 'POST',
                        data: {
                            _token: "{{ csrf_token() }}",
                            address: $(document).find('#rc_addr').text(),
                        },
                        success: function (response) {
                        },
                        error: function () {
                            Swal.close();
                            Swal.fire({
                                title: 'Atenção',
                                text: "Não conseguimos confirmar seu pagamento. Atualize a ppagina e tente novamente!",
                                type: 'error',
                            }).then((result) => {
                                if (result.value || result.dismiss) {
                                    window.location.reload(true);
                                }
                            });
                        }
                    });
                }
            });
            $('#fase_50').on({
                show: function () {
                    Swal.fire({
                        title: 'Aguarde...',
                        html: 'Estamos processando seu pagamento',
                        allowOutsideClick: false,
                        onBeforeOpen: () => {
                            Swal.showLoading();
                        },
                        onOpen: () => {
                            $.ajax({
                                url: '{{ route('pagamento.braziliex.callback', [ $pedido->id ]) }}',
                                type: 'GET',
                                data: {
                                    address: $(document).find('#rc_addr').text(),
                                },
                                success: function (response) {
                                    Swal.fire({
                                        title: 'Atenção',
                                        text: 'Seu pagamento foi confirmado e já depositamos o saldo em sua conta!',
                                        type: 'success'
                                    }).then((result) => {
                                        if (result.value || result.dismiss) {
                                            window.location.href = '{{ route('deposito.depositos.usuario', [Auth::user()->id]) }}';
                                        }
                                    });
                                },
                                error: function () {
                                    Swal.close();
                                    Swal.fire({
                                        title: 'Atenção',
                                        text: "Não conseguimos confirmar seu pagamento. Se o pagamento foi efetuado entre em contato conosco.",
                                        type: 'error',
                                    }).then((result) => {
                                        if (result.value || result.dismiss) {
                                            window.location.reload(true);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        });
    </script>
@endsection